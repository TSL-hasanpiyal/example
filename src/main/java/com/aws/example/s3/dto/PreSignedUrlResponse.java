package com.aws.example.s3.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class PreSignedUrlResponse {
    List<String> presignedUrls;
    private String uploadId;
}