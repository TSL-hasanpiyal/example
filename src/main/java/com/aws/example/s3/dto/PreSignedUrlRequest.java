package com.aws.example.s3.dto;

import lombok.Data;

@Data
public class PreSignedUrlRequest {
    private String filename;
    private String contentType;
    private int totalParts;
}