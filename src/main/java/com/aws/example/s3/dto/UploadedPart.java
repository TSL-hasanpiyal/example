package com.aws.example.s3.dto;

import lombok.Data;

@Data
public class UploadedPart {
    private int partNumber;
    private String tag;
}
