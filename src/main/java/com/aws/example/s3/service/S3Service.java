package com.aws.example.s3.service;

import com.amazonaws.HttpMethod;
import com.amazonaws.services.s3.model.PartETag;
import com.aws.example.s3.dto.PreSignedUrlRequest;
import com.aws.example.s3.dto.PreSignedUrlResponse;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

public interface S3Service {
    String uploadFile(String bucketName, String fileName, MultipartFile file) throws IOException;

    String getPreSignedUrl(String bucketName, String fileName, HttpMethod httpMethod);

    String deleteFile(String bucketName, String fileName);

    PreSignedUrlResponse getMultipartPreSignedUrls(String bucketName, PreSignedUrlRequest request);

    void assembleParts(String bucketName, String objectKey, String uploadId, List<PartETag> partETags);
}
