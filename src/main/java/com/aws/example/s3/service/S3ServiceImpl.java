package com.aws.example.s3.service;

import com.amazonaws.HttpMethod;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.*;
import com.aws.example.s3.dto.PreSignedUrlRequest;
import com.aws.example.s3.dto.PreSignedUrlResponse;
import com.aws.example.util.PropertiesData;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
@Slf4j
@RequiredArgsConstructor
public class S3ServiceImpl implements S3Service {

    private final PropertiesData propertiesData;

    private final AmazonS3 s3Client;

    @Override
    public String uploadFile(String bucketName, String fileName, MultipartFile file) throws IOException {
        ObjectMetadata objectMetadata = new ObjectMetadata();
        objectMetadata.setContentLength(file.getSize());
        objectMetadata.setContentType("application/octet-stream");

        s3Client.putObject(new PutObjectRequest(bucketName, fileName, file.getInputStream(), objectMetadata));
        return "File Uploaded successfully";
    }

    @Override
    public String getPreSignedUrl(String bucketName, String fileName, HttpMethod httpMethod) {
        Date expiration = new Date();

        long expTimeMillis = expiration.getTime();
        expTimeMillis += propertiesData.getAwsPreSignedUrlExpiration();
        expiration.setTime(expTimeMillis);

        GeneratePresignedUrlRequest generatePresignedUrlRequest =
                new GeneratePresignedUrlRequest(bucketName, fileName)
                        .withMethod(httpMethod)
                        .withExpiration(expiration);

        URL url = s3Client.generatePresignedUrl(generatePresignedUrlRequest);
        log.info("Pre-signed url generated as: {}", url.toString());
        return url.toString();
    }

    @Override
    public String deleteFile(String bucketName, String fileName) {
        s3Client.deleteObject(new DeleteObjectRequest(bucketName, fileName));
        return "File Deleted successfully";
    }

    @Override
    public PreSignedUrlResponse getMultipartPreSignedUrls(String bucketName, PreSignedUrlRequest request) {
        InitiateMultipartUploadRequest initRequest = new InitiateMultipartUploadRequest(bucketName, request.getFilename());
        InitiateMultipartUploadResult initResponse = s3Client.initiateMultipartUpload(initRequest);
        String uploadId = initResponse.getUploadId();

        List<String> preSignedUrls = new ArrayList<>();

        for (int partNumber = 0; partNumber < request.getTotalParts(); partNumber++) {
            Date expiration = new Date();
            long expTimeMillis = expiration.getTime();
            expTimeMillis += propertiesData.getAwsPreSignedUrlExpiration();
            expiration.setTime(expTimeMillis);

            GeneratePresignedUrlRequest generatePresignedUrlRequest =
                    new GeneratePresignedUrlRequest(bucketName, request.getFilename())
                            .withMethod(HttpMethod.PUT)
                            .withExpiration(expiration)
                            .withContentType(request.getContentType());
            generatePresignedUrlRequest.addRequestParameter("uploadId", uploadId);
            generatePresignedUrlRequest.addRequestParameter("partNumber", String.valueOf(partNumber + 1));

            preSignedUrls.add(s3Client.generatePresignedUrl(generatePresignedUrlRequest).toString());
        }
        return new PreSignedUrlResponse(preSignedUrls, uploadId);
    }

    public void assembleParts(String bucketName, String objectKey, String uploadId, List<PartETag> partETags) {
        CompleteMultipartUploadRequest completeRequest = new CompleteMultipartUploadRequest(
                bucketName, objectKey, uploadId, partETags);
        s3Client.completeMultipartUpload(completeRequest);
    }
}
