package com.aws.example.util;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class CommonFunction {

    public static String getFileExtension(String filename) {
        int lastDotIndex = filename.lastIndexOf(".");
        if (lastDotIndex > 0 && lastDotIndex < filename.length() - 1) {
            return "." + filename.substring(lastDotIndex + 1);
        }
        return "";
    }

    public static String getHostname() {
        try {
            // Get the InetAddress of the local machine (EC2 instance)
            InetAddress address = InetAddress.getLocalHost();
            // Get the hostname
            String hostname = address.getHostName();
            return "Hostname: " + hostname;
        } catch (UnknownHostException e) {
            e.printStackTrace();
            return "Error retrieving hostname";
        }
    }
}
