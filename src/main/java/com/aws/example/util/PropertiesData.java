package com.aws.example.util;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@Data
public class PropertiesData {

    @Value("${asset.baseurl}")
    private String assetsBaseUrl;

    @Value("${aws.accessKeyId}")
    private String awsAccessKey;

    @Value("${aws.secretKey}")
    private String awsSecretKey;

    @Value("${aws.region}")
    private String awsRegion;

    @Value("${aws.s3.public.bucket}")
    private String awsS3PublicBucket;

    @Value("${aws.s3.preSignedurl.expiration}")
    private long awsPreSignedUrlExpiration;

}
