package com.aws.example.util;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.config.MethodInvokingFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Properties;

@Configuration
@RequiredArgsConstructor
public class SystemPropertiesConfig {

    public static final String ASSETS_BASE_URL = "ASSETS_BASE_URL";
    private final PropertiesData propertiesData;

    private Properties getAllProperties() {
        Properties pros = new Properties();
        pros.setProperty(ASSETS_BASE_URL, propertiesData.getAssetsBaseUrl());
        return pros;
    }

    @Bean
    public MethodInvokingFactoryBean systemPropertiesBean() {
        MethodInvokingFactoryBean bean = new MethodInvokingFactoryBean();
        bean.setTargetObject(System.getProperties());
        bean.setTargetMethod("putAll");
        bean.setArguments(new Object[]{getAllProperties()});
        return bean;
    }
}
