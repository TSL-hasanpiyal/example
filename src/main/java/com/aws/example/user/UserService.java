package com.aws.example.user;

import com.aws.example.s3.service.S3Service;
import com.aws.example.util.PropertiesData;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

import static com.aws.example.util.CommonFunction.getFileExtension;

@Service
@Slf4j
@RequiredArgsConstructor
public class UserService {

    private final UserRepository userRepository;
    private final S3Service s3Service;
    private final PropertiesData propertiesData;

    public User save(User user) {
        MultipartFile image = user.getImage();
        user = userRepository.save(user);
        try {
            if (image.getOriginalFilename() != null) {
                String fileName = "application/user/" + user.getId() + "/" + System.currentTimeMillis() + getFileExtension(image.getOriginalFilename());
                s3Service.uploadFile(propertiesData.getAwsS3PublicBucket(), fileName, user.getImage());
                log.info("FILE UPLOADED ON S3: " + fileName);
                user.setImageS3(fileName);
                userRepository.save(user);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        user.setImage(null);
        return user;
    }

    public List<User> findAll() {
        return userRepository.findAll();
    }

    public void delete(Long id) {
        userRepository.deleteById(id);
    }


}
