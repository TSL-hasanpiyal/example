package com.aws.example.user;

import com.aws.example.util.SystemPropertiesConfig;
import jakarta.persistence.*;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

@Data
@Entity
@Table(name = "users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;
    private String email;
    private String zone;
    private String imageNfs;
    private String imageS3;

    @Transient
    private MultipartFile image;

    public String getImageS3() {
        if (imageS3 != null && !imageS3.isEmpty()) {
            return System.getProperty(SystemPropertiesConfig.ASSETS_BASE_URL) + imageS3;
        }
        return "";
    }
}
