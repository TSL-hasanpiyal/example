package com.aws.example.controller;

import com.aws.example.user.User;
import com.aws.example.user.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static com.aws.example.util.CommonFunction.getHostname;

@Controller
@RequestMapping("")
@Slf4j
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;


    @GetMapping("/users")
    public String getUsers(Model model) {
        model.addAttribute("hostname", getHostname());
        return "user/index";
    }

    @PostMapping("/user/save")
    @ResponseBody
    public ResponseEntity<User> save(User user) {
        user = userService.save(user);
        return new ResponseEntity<>(user, HttpStatus.OK);
    }

    @DeleteMapping("/user/delete/{id}")
    @ResponseBody
    public ResponseEntity<Boolean> delete(@PathVariable Long id) {
        userService.delete(id);
        return new ResponseEntity<>(true, HttpStatus.OK);
    }

    @GetMapping("/user/get-list")
    @ResponseBody
    public ResponseEntity<List<User>> getList() {
        return new ResponseEntity<>(userService.findAll(), HttpStatus.OK);
    }


}
