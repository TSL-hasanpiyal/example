package com.aws.example.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import static com.aws.example.util.CommonFunction.getHostname;

@Controller
@RequestMapping("")
@Slf4j
@RequiredArgsConstructor
public class HomeController {

    @GetMapping("")
    public String index(Model model) {
        model.addAttribute("hostname", getHostname());
        return "index";
    }

    @GetMapping("/hostname")
    @ResponseBody
    public String hostname() {
        return getHostname();
    }

}
